###### Deployed App From Private Docker Registry ######

##### Demo Project: ######
Deploy web application in K8s cluster from private
Docker registry
#### Technologies used: ####
Kubernetes, Helm, AWS ECR, Docker

#### Project Description ####
Create Secret for credentials for the private Docker
registry

Configure the Docker registry secret in application
Deployment component

Deploy web application image from our private Docker
registry in K8s cluster